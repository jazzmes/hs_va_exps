__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2015, Haystack TV Inc.'
__version__ = '0.1'
__email__ = 'tiago@haystack.tv'

total = 133
users = 6
primary = 0

combs = [(i, j) for i in range(users) for j in range(i, users) if i != j]

print(combs)
c = 0
for i in range(users):

    indexes = []
    for j in range(total):
        if i in combs[c]:
            indexes.append(j)
        c = (c + 1) % len(combs)
    print("User %d : %s = %d" % (i, indexes, len(indexes)))
