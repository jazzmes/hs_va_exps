import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hs_va_exps.settings")
import sys
sys.path.append('../../videoanalysis')
import django
django.setup()

from django.db import transaction
from silences.models import Video, Silence
from videoanalysis.db.mongo import MongoDB
from videoanalysis.aws.s3 import S3VideoAnalysisTestbed
from videoanalysis.learn.datasets import VideoDataset
from videoanalysis.analysis.silence_detector import SilenceDetector

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2015, Haystack TV Inc.'
__version__ = '0.1'
__email__ = 'tiago@haystack.tv'


def run_test(levels, limit=1000, ds_name=None, base_dir='.'):
    # create db connection
    print(" * Connect to DB")
    db = MongoDB(
        host="ec2-54-172-106-255.compute-1.amazonaws.com",
        user="carmelotest",
        password="sgdygdshdsh4262727",
        db="haystackdb"
    )
    db.connect()
    print("Collections: %s" % db.conn.collection_names())

    # create/load storage connection
    print(" * Connect to S3")
    s3 = S3VideoAnalysisTestbed(
        aws_id="AKIAJR2EWT7ITAMM4DFA",
        aws_key="kbr3VIN+f+t0RpjuCzD9rOAOjNxjXdOkJmXC46AL"
    )

    # create dataset
    print(" * Create/load dataset")
    ds = VideoDataset(db, s3, soft=True, name=ds_name, base_dir=base_dir)
    ds.load_dataset(limit)

    # load every video from the test database
    print(" * Get records to process")
    print("Number of records: %d" % len(ds))

    # transaction.set_autocommit(False)
    for idx, video in enumerate(ds):
        print("Processing video [%5d]: %s" % (idx, video.url))
        filename = ds.get_audio(video.url)
        if filename:
            print("\tDetecting silences - filename: %s" % filename)
            vid = None
            for level in levels:
                result = SilenceDetector.run_silence_filter(filename, min_level=level)
                if result[0]:
                    print("\tResult: %s" % str(result))
                    if not vid:
                        vid, created = Video.objects.get_or_create(url=video.url, defaults={'duration': result[0]})
                        print(vid.silence_set.all())
                        vid.silence_set.all().delete()
                        print(vid.silence_set.all())
                        vid.save()
                        # transaction.commit()
                    if result and isinstance(result, tuple):
                        if result[1]:
                            print("Video with silence: %s" % video.url)
                            print("DB :: ADD SILENCES (level = %d)" % level)
                            for s in result[1]:
                                print("Key: %s, %s, %s" % (vid.id, s[0], level))
                                Silence(
                                    video=vid,
                                    start=s[0] if s[0] > 0 else 0,
                                    end=s[1] if s[1] > 0 else result[0],
                                    noise_margin=level
                                ).save()
    # transaction.commit()
    # close database
    db.close()

    return


if __name__ == "__main__":
    run_test(range(-50, 0, 10), ds_name="testing", base_dir="../")
