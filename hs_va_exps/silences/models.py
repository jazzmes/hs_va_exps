from django.db import models
from django.contrib.auth.models import User


class VideoCategory(models.Model):

    name = models.CharField(max_length=50)
    group = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Video(models.Model):

    url = models.URLField(unique=True)
    description = models.CharField(max_length=512, null=True)
    duration = models.IntegerField()

    remove = models.BooleanField(default=False)
    bad_quality = models.BooleanField(default=False)
    inadequate_content = models.BooleanField(default=False)
    inadequate_to_listen = models.BooleanField(default=False)
    specific_content = models.BooleanField(default=False)
    notes = models.TextField(blank=True, default='')

    def __str__(self):
        return self.url


class Silence(models.Model):

    video = models.ForeignKey(Video)
    start = models.FloatField()
    end = models.FloatField()
    noise_margin = models.IntegerField()

    class Meta:
        unique_together = ('video', 'start', 'noise_margin')


class Classification(models.Model):

    user = models.ForeignKey(User)
    video = models.ForeignKey(Video)
    categories = models.ManyToManyField(VideoCategory)
    comment = models.TextField(blank=True, default='')

    class Meta:
        unique_together = ('user', 'video')