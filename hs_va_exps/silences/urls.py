from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    # url(r'^videos/([0-9]{4})/$', views.video, name='video')
    url(r'^video/([0-9]+)/$', views.video, name='video'),
    url(r'^video_notes/([0-9]+)/$', views.video_notes, name='video_notes'),
    url(r'^videos/([a-zA-Z]+)/$', views.videos, name='videos'),
    url(r'^videos_by_category/([0-9]+)/$', views.videos_by_category, name='videos_by_category'),
    url(r'^stats/$', views.stats, name='stats'),
    url(r'^links/$', views.links, name='links')
]