import hashlib
import logging
import traceback
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseNotFound
from django.template.context import RequestContext
from django.template.loader import render_to_string
from graphos.sources.simple import SimpleDataSource
from graphos.renderers.flot import LineChart
from .forms import VideoShortNotesForm, VideoClassifyForm
from .models import Video, VideoCategory, Classification

KEY = 'S/4\\1C.I9Btml:Q}tG9:P\\\'e'

logger = logging.getLogger(__name__)


def index(request):

    videos = Video.objects.all()
    levels = range(-50, 0, 10)

    # percentages
    totals_perc = {l: {"has": 0, "starts": 0, "ends": 0} for l in levels}
    videos_per_level = {l: [] for l in (list(levels) + [0])}

    for v in videos:
        added = False
        totals_per_level = {l: [] for l in levels}
        for l in range(-50, 0, 10):
            cnt = v.silence_set.filter(noise_margin=l).count()
            totals_per_level[l] = cnt
            if cnt:
                totals_perc[l]["has"] += 1

                if not added:
                    videos_per_level[l].append(v)
                    added = True

            if any([s.start < 0.1 for s in v.silence_set.filter(noise_margin=l)]):
                totals_perc[l]["starts"] += 1
            if any([int(s.end) == int(v.duration) for s in v.silence_set.filter(noise_margin=l)]):
                totals_perc[l]["ends"] += 1

        if not added:
            videos_per_level[0].append(v)
        v.totals_per_level = ", ".join(["%ddB: %d" % (l, totals_per_level[l]) for l in levels])
        v.video_id = v.url.split("/")[-1]

    data_perc = [
        ['Level', 'Perc w/ Silence', 'Perc Ends w/ Silence', 'Perc Start w/ Silence']
    ]
    for l in levels:
        data_perc.append([
            l,
            float(totals_perc[l]["has"]) / len(videos) * 100,
            float(totals_perc[l]["ends"]) / len(videos) * 100,
            float(totals_perc[l]["starts"]) / len(videos) * 100
        ])
    chart_perc = LineChart(
        SimpleDataSource(data=data_perc),
        html_id="totals_perc",
        options={
            'lines': {
                'fill': True
            },
            'xaxis': {
                'axisLabel': 'Noise Tolerance (dB)',
                'ticks': levels

            },
            'yaxis': {
                'axisLabel': 'Percentage of Videos'
            }
        }
    )

    # l = -30
    # for

    context = {
        "title": "Results - Silence Detection",
        "ajax": request.is_ajax(),
        "chart_perc": chart_perc,
        "videos": [v for l in (list(levels) + [0]) for v in videos_per_level[l]]
    }
    return render(request, 'index.html', context)


def get_token(username):
    return hashlib.sha256(KEY + username).hexdigest()


def get_overlapping_indexes(total, users, user_index):

    combs = [(i, j) for i in range(users) for j in range(i, users) if i != j]

    c = 0
    indexes = []
    for j in range(total):
        if user_index in combs[c]:
            indexes.append(j)
        c = (c + 1) % len(combs)

    return indexes


def videos(request, username):
    #simple check before login is implemented
    token = request.GET['token'] if 'token' in request.GET else None

    if get_token(username) != token:
        return HttpResponseNotFound('<h1>Page not found - Bad Token</h1>')

    u = User.objects.get(username=username)
    users = list(User.objects.all())
    i = users.index(u)

    all_videos = Video.objects.all()
    indexes = get_overlapping_indexes(len(all_videos), len(users), i)

    user_videos = []
    for j in indexes:
        v = all_videos[j]
        v.video_id = v.url.split("/")[-1]
        v.silence_count = len(v.silence_set.filter(noise_margin=-40))
        try:
            c = v.classification_set.get(user=u)
            v.categories = c.categories
            v.comment = c.comment
        except Classification.DoesNotExist:
            v.categories = []
            v.comment = None
        user_videos.append(v)

    context = {
        "title": "List of Videos",
        "ajax": request.is_ajax(),
        "videos": user_videos,
        "username": username
    }

    if request.is_ajax():
        return HttpResponse(render_to_string('videos.html', context))
    else:
        return render(request, 'videos.html', context)


def use_video_short_notes_form(request, v, context):
    # process form
    if request.method == 'POST':
        form = VideoShortNotesForm(request.POST)
        if form.is_valid():
            v.notes = form.cleaned_data["notes"]
            v.remove = form.cleaned_data["remove"]
            v.bad_quality = form.cleaned_data["bad_quality"]
            v.inadequate_content = form.cleaned_data["inadequate_content"]
            v.specific_content = form.cleaned_data["specific_content"]
            v.inadequate_to_listen = form.cleaned_data["inadequate_to_listen"]
            v.save()
            result = True
        else:
            print("FORM INVALID")
            result = False

        if request.is_ajax():
            data = {
                'result': result,
            }
            return JsonResponse(data)
    else:
        form = VideoShortNotesForm({k: getattr(v, k) for k in ["remove", "notes", "bad_quality",
                                                               "inadequate_content", "specific_content",
                                                               "inadequate_to_listen"]})
    context.update({"form": form})


def use_video_classify_form(request, v, context):
    # process form
    # print("in classify form: %s" % request.method)
    if request.method == 'POST':
        msg = ""
        result = False
        try:
            form = VideoClassifyForm(request.POST)
            # print("fields")
            # print(form.fields)
            if form.is_valid():
                cats = []
                for k, val in form.cleaned_data.items():
                    if k.startswith("categories"):
                        cats += val

                u = User.objects.get(username=request.POST["username"])
                try:
                    c = v.classification_set.get(user=u)
                except Classification.DoesNotExist:
                    c = Classification(user=u, video=v)
                    c.save()
                    v.classification_set.add(c)

                c.categories = cats
                c.comment = form.cleaned_data["comment"]
                c.save()
                v.save()
                result = True
            else:
                print("FORM INVALID")
                result = False
                msg = "Invalid form"
        except:
            print("Excepting: %s" % traceback.format_exc())
            result = False
            msg = traceback.format_exc()

        if request.is_ajax():
            data = {
                'result': result,
                'msg': msg
            }
            return JsonResponse(data)
    else:
        if "username" not in request.GET:
            raise NotImplementedError()

        user = User.objects.get(username=request.GET["username"])

        groups = {v.group for v in VideoCategory.objects.all()}
        current = {}
        try:
            c = v.classification_set.get(user=user)
            current["comment"] = c.comment
            for g in groups:
                current["categories_%s" % g] = c.categories.filter(group=g)
        except Classification.DoesNotExist:
            pass

        form = VideoClassifyForm(current)
        context.update({"form": form})


def video(request, video_id, classify=True):
    v = Video.objects.get(id=video_id)

    context = {
        "title": "Video Detail",
        "ajax": request.is_ajax()
    }
    if classify:
        resp = use_video_classify_form(request, v, context)
    else:
        resp = use_video_short_notes_form(request, v, context)
    if resp:
        return resp

    v.video_id = v.url.split("/")[-1]
    v.silences_selected = ["From %.1fs to %.1fs" % (s.start, s.end) for s in v.silence_set.filter(noise_margin=-50)]
    v.silences_by_level = []
    for l in range(-50, 0, 10):
        v.silences_by_level.append(
            {
                "level": l,
                "count": v.silence_set.filter(noise_margin=l).count(),
                "silences": ", ".join(["[%.1f, %.1f]" % (s.start, s.end) for s in v.silence_set.filter(noise_margin=l)])
            }
        )

    context["video"] = v
    context["classification"] = classify
    if request.is_ajax():
        return HttpResponse(render_to_string('video.html', RequestContext(request, context)))
    else:
        return render(request, 'video.html', context)


def links(request):
    t = {}
    for u in User.objects.all():
        t[u.username] = get_token(u.username)
    return render(request, 'links.html', {"tokens": t})


def video_notes(request, video_id):
    return video(request, video_id, classify=False)


def stats(request):
    classifications = {}
    categories = {}
    users = {}

    disagreements = {}
    total_disagreements = {}

    for v in Video.objects.all():
        nc = v.classification_set.count()
        classifications.setdefault(nc, 0)
        classifications[nc] += 1

        cs = set()
        for c in v.classification_set.all():
            for cat in c.categories.all():
                cs.add(cat.name)

            users.setdefault(c.user.username, 0)
            users[c.user.username] += 1

        for cat_name in cs:
            categories.setdefault(cat_name, 0)
            categories[cat_name] += 1

        num_users = v.classification_set.count()
        num_users_category = {}
        for c in v.classification_set.all():
            for cat in c.categories.all():
                if cat.name in num_users_category:
                    num_users_category[cat.name] += 1
                else:
                    num_users_category[cat.name] = 1

        for cat_name, val in num_users_category.items():
            if cat_name in total_disagreements:
                total_disagreements[cat_name] += 1
            else:
                total_disagreements[cat_name] = 1

            if val and val != num_users:
                if cat_name in disagreements:
                    disagreements[cat_name] += 1
                else:
                    disagreements[cat_name] = 1

    cs = VideoCategory.objects.all().order_by('group', 'name')
    for c in cs:
        c.cnt = categories[c.name] if c.name in categories else 0
        c.disagreements = disagreements[c.name] if c.name in disagreements else 0
        c.total_disagreements = total_disagreements[c.name] if c.name in total_disagreements else 0

    return render(request, 'stats.html', {
        "classifications": classifications,
        "categories": cs,
        "users": users,
        "total": Video.objects.count(),
        "total_classified": sum([v for k, v in classifications.items() if k > 0]),
        "total_2_or_more": sum([v for k, v in classifications.items() if k > 1])
    })


def videos_by_category(request, category_name):
    vc = VideoCategory.objects.get(id=category_name)
    classifications = Classification.objects.filter(categories=vc)
    vs = {c.video for c in classifications}

    for v in vs:
        v.video_id = v.url.split("/")[-1]
        try:
            v.categories = set()
            v.number_of_users = v.classification_set.count()
            v.first_user = None
            v.other_users = []
            for c in v.classification_set.all():
                for cat in c.categories.all():
                    v.categories.add(cat.name)
                if v.first_user:
                    v.other_users.append({
                        "username": c.user.username,
                        "categories": [cat.name for cat in c.categories.all()],
                        "comment": c.comment
                    })
                else:
                    v.first_user = {
                        "username": c.user.username,
                        "categories": [cat.name for cat in c.categories.all()],
                        "comment": c.comment
                    }

        except Classification.DoesNotExist:
            pass
        v.categories = list(v.categories)

    return render(request, 'videos_by_category.html', {"videos": vs})


# def analysis(request):
#     # 5 graphs
#     # total duration
#     # average duration
#     # number of periods
#     # start duration
#     # end duration
#
#     total_duration_points = []
#     avg_duration_points = []
#     relative_total_duration_points = []
#     number_periods_points = []
#     start_duration_points = []
#     end_duration_points = []
#     mid_duration_points = []
#
#     videos = {s.video for s in Silence.objects.filter(noise_margin=-50)}
#     v_features = []
#
#     for v in videos:
#         print("video url: %s" % v.url)
#         total = 0
#         cnt = 0
#         sp = 0
#         ep = 0
#         mid = 0
#         print("silences: %s" % [(s.start, s.end) for s in v.silence_set.filter(noise_margin=-50)])
#         for s in v.silence_set.filter(noise_margin=-50):
#             duration = s.end - s.start
#             total += duration
#             cnt += 1
#             if s.start < 0.1:
#                 sp = duration
#             elif not s.end == v.duration:
#                 mid += duration
#             if s.end == v.duration:
#                 ep = duration
#
#         # lbl = v.bad_quality
#         # lbl = v.remove
#         # lbl = v.bad_quality or v.inadequate_content
#         lbl = v.bad_quality or v.inadequate_to_listen or v.inadequate_content
#         total_duration_points.append((total, lbl))
#         avg_duration_points.append((total/cnt, lbl))
#         relative_total_duration_points.append((total/v.duration, lbl))
#         number_periods_points.append((cnt, lbl))
#         start_duration_points.append((sp, lbl))
#         end_duration_points.append((ep, lbl))
#         mid_duration_points.append((mid, lbl))
#
#         print("total: %s" % total)
#
#         v_features.append({
#             "label": lbl,
#             "id": v.id,
#             "url": v.url,
#             "video_id": v.url.split("/")[-1],
#             # total_duration, avg_duration, realtive_durationc, period_count, start_duration, end_duration, mid duration
#             "data": [total, total/cnt, total/v.duration, cnt, sp, ep, mid]
#         })
#
#     print("Learning from data...")
#     random.shuffle(v_features)
#
#     # numpy data
#     data = np.array([v["data"] for v in v_features])
#     target = np.array([1 if v["label"] else 0 for v in v_features])
#
#     auto_tree = True
#
#     if auto_tree:
#         # **** START: learning from automatic decision tree
#         training_index = int(len(data) * 0.8)
#         # learn and predict
#         la = DecisionTree(
#         # la = LogReg(
#             data[0:training_index], target[0:training_index],
#             # features=[0, 1, 2, 3, 4, 5, 6] #,3,4,5,6]
#             min_samples_leaf=math.ceil(0.05 * training_index)
#         )
#         la.learn()
#         print("Classifier ready!")
#         print("train=%d" % len(data[0:training_index]))
#         print("test=%d" % len(data[training_index:]))
#
#         # not correct: applying to the training data
#         test_data = data[training_index:]
#         predicted_labels = la.predict(test_data)
#
#         # output tree
#         la.output_tree()
#         # **** END: learning from automatic decision tree
#     else:
#         la = ManualDecisionTree(data, target)
#         la.learn()
#         test_data = data
#         # predicted_labels = la.predict(data)
#         training_index = 0
#
#     success = 0
#     false_positive = 0
#     false_negative = 0
#     true_positive = 0
#     for i in range(len(test_data)):
#         features = np.array([v_features[training_index + i]["data"]])
#         label = v_features[training_index + i]["label"]
#         predicted = la.predict(features)[0]
#         v_features[training_index + i]["prediction"] = predicted
#         if predicted == label:
#             if predicted:
#                 true_positive += 1
#             success += 1
#         elif predicted:
#             false_positive += 1
#         else:
#             false_negative += 1
#
#             # v_features[training_index + i]["prediction"] = predicted_labels[i]
#             # if predicted_labels[i] == v_features[training_index + i]["label"]:
#             #     success += 1
#             # elif predicted_labels[i]:
#             #     false_positive += 1
#             # else:
#             #     false_negative += 1
#
#     return render(request, 'analysis.html', {
#         "features": [
#             ["total_duration", sorted(total_duration_points, cmp=lambda x, y: cmp(x[0], y[0]))],
#             ["avg_duration", sorted(avg_duration_points, cmp=lambda x, y: cmp(x[0], y[0]))],
#             ["relative_duration", sorted(relative_total_duration_points, cmp=lambda x, y: cmp(x[0], y[0]))],
#             ["number_periods", sorted(number_periods_points, cmp=lambda x, y: cmp(x[0], y[0]))],
#             ["start_duration", sorted(start_duration_points, cmp=lambda x, y: cmp(x[0], y[0]))],
#             ["end_duration",  sorted(end_duration_points, cmp=lambda x, y: cmp(x[0], y[0]))],
#             ["mid_duration",  sorted(mid_duration_points, cmp=lambda x, y: cmp(x[0], y[0]))],
#         ],
#         "videos": v_features,
#         "success_rate": float(success)/len(test_data) * 100,
#         "false_positive_rate": float(false_positive)/len(test_data) * 100,
#         "false_negative_rate": float(false_negative)/len(test_data) * 100,
#         "success": success,
#         "false_positive": false_positive,
#         "false_negative": false_negative,
#         "precision": 100 * float(true_positive) / (true_positive + false_positive) if true_positive + false_positive else -1,
#         "recall": 100 * float(true_positive) / (true_positive + false_negative) if true_positive + false_negative else -1
#     })


