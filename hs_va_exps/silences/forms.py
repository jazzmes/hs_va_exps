from django import forms
from .models import Video, VideoCategory

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2015, Haystack TV Inc.'
__version__ = '0.1'
__email__ = 'tiago@haystack.tv'


class VideoShortNotesForm(forms.ModelForm):

    class Meta:
        model = Video
        fields = ['remove', 'notes', 'bad_quality', 'specific_content', 'inadequate_content', 'inadequate_to_listen']


# class VideoClassifyForm(forms.ModelForm):
#
#     class Meta:
#         model = Video
#         fields = ['comment', 'categories']


class VideoClassifyForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea, required=False)

    def __init__(self, *args, **kwargs):
        super(VideoClassifyForm, self).__init__(*args, **kwargs)

        groups = {v.group for v in VideoCategory.objects.all()}
        for g in groups:
            self.fields["categories_%s" % g] = forms.ModelMultipleChoiceField(
                queryset=VideoCategory.objects.filter(group=g).order_by('name'),
                widget=forms.CheckboxSelectMultiple(),
                required=False
            )
