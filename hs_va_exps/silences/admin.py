from django.contrib import admin
from .models import Video, Silence, VideoCategory, Classification


class VideoCategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)


class VideoAdmin(admin.ModelAdmin):
    list_display = ('url', 'duration')


class SilenceAdmin(admin.ModelAdmin):
    list_display = ('video', 'noise_margin', 'start', 'end')


class ClassificationAdmin(admin.ModelAdmin):
    list_display = ('user', 'video', 'comment')


admin.site.register(VideoCategory, VideoCategoryAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Silence, SilenceAdmin)
admin.site.register(Classification, ClassificationAdmin)