# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('silences', '0010_videocategory_group'),
    ]

    operations = [
        migrations.CreateModel(
            name='Classification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.TextField(default=b'', blank=True)),
                ('categories', models.ManyToManyField(to='silences.VideoCategory')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='video',
            name='categories',
        ),
        migrations.RemoveField(
            model_name='video',
            name='comment',
        ),
        migrations.AddField(
            model_name='classification',
            name='video',
            field=models.ForeignKey(to='silences.Video'),
        ),
        migrations.AlterUniqueTogether(
            name='classification',
            unique_together=set([('user', 'video')]),
        ),
    ]
