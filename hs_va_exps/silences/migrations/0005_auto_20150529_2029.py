# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('silences', '0003_auto_20150529_1932'),
    ]

    operations = [
        migrations.AlterField(
            model_name='silence',
            name='end',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='silence',
            name='start',
            field=models.FloatField(),
        ),
    ]
