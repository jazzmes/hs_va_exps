# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('silences', '0009_auto_20150603_2324'),
    ]

    operations = [
        migrations.AddField(
            model_name='videocategory',
            name='group',
            field=models.CharField(default='Target', max_length=50),
            preserve_default=False,
        ),
    ]
