# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Silence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.IntegerField()),
                ('end', models.IntegerField()),
                ('noise_margin', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
                ('description', models.CharField(max_length=512)),
                ('duration', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='silence',
            name='video',
            field=models.ForeignKey(to='silences.Video'),
        ),
        migrations.AlterUniqueTogether(
            name='silence',
            unique_together=set([('video', 'start', 'noise_margin')]),
        ),
    ]
