# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('silences', '0008_video_specific_content'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='video',
            name='comment',
            field=models.TextField(default=b'', blank=True),
        ),
        migrations.AddField(
            model_name='video',
            name='categories',
            field=models.ManyToManyField(to='silences.VideoCategory'),
        ),
    ]
