# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('silences', '0005_auto_20150529_2029'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='notes',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='video',
            name='remove',
            field=models.BooleanField(default=False),
        ),
    ]
