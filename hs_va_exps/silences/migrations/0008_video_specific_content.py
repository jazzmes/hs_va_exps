# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('silences', '0007_auto_20150603_1926'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='specific_content',
            field=models.BooleanField(default=False),
        ),
    ]
