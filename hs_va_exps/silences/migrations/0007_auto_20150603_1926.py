# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('silences', '0006_auto_20150603_1646'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='bad_quality',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='video',
            name='inadequate_content',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='video',
            name='inadequate_to_listen',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='video',
            name='notes',
            field=models.TextField(default=b'', blank=True),
        ),
    ]
